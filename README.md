# Vagrant環境でfluentd + elasticsearch + kibana ためしたみた

Dockerのほうがお手軽なんだろうけど、セットアップするところからさらいたかったので。

* fluentdでapacheのログをelasticsearchに送る

## 起動

```bash
$ vagrant up
```

## 動作確認

Apache以外はデフォルトのポートをフォワードしている。

### Apache

http://localhost:8080

わざとエラーを起こすアプリ。[monolog](https://github.com/Seldaek/monolog)のSocketHandlerとFluentdFormatterを使ってエラーログを送りつける。
http://localhost:8080/app/public

### Elasticsearch

http://localhost:9200

```bash
$ curl http://localhost:9200
{
  "name" : "nTJ6rxM",
  "cluster_name" : "elasticsearch",
  "cluster_uuid" : "zvBGngjoQwKKLSV1I1NIKA",
  "version" : {
    "number" : "6.1.2",
    "build_hash" : "5b1fea5",
    "build_date" : "2018-01-10T02:35:59.208Z",
    "build_snapshot" : false,
    "lucene_version" : "7.1.0",
    "minimum_wire_compatibility_version" : "5.6.0",
    "minimum_index_compatibility_version" : "5.0.0"
  },
  "tagline" : "You Know, for Search"
}
```

### kibana

http://localhost:5601
