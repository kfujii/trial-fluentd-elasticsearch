#!/bin/sh

export DEBIAN_FRONTEND=noninteractive

echo "Asia/Tokyo" > /etc/timezone && dpkg-reconfigure -f noninteractive tzdata

sed -i'~' -E "s@http://(..\.)?(archive|security)\.ubuntu\.com/ubuntu@http://ftp.jaist.ac.jp/pub/Linux/ubuntu@g" /etc/apt/sources.list

apt update
apt install -y apache2 php libapache2-mod-php php-xml curl ntp openjdk-8-jdk apt-transport-https

## install fluentd
## see. https://docs.fluentd.org/v1.0/articles/install-by-deb
curl -L https://toolbelt.treasuredata.com/sh/install-ubuntu-xenial-td-agent2.sh | sh
usermod -aG adm td-agent
usermod -aG adm vagrant
/etc/init.d/td-agent restart

## install Elasticsearch & kibana
## see. https://www.elastic.co/guide/en/elasticsearch/reference/current/deb.html
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add -

if [ ! -e /etc/apt/sources.list.d/elastic-6.x.list ]; then
    echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | tee -a /etc/apt/sources.list.d/elastic-6.x.list
fi

apt-get update
apt-get install -y elasticsearch kibana
systemctl daemon-reload
systemctl enable elasticsearch.service
systemctl enable kibana.service
systemctl start elasticsearch.service
systemctl start kibana.service
usermod -aG elasticsearch vagrant

td-agent-gem install fluent-plugin-elasticsearch

## configs
cp /vagrant/settings/td-agent/td-agent.conf /etc/td-agent/
cp /vagrant/settings/apache2/000-default.conf /etc/apache2/sites-available/
cp /vagrant/settings/elasticsearch/elasticsearch.yml /etc/elasticsearch/
cp /vagrant/settings/kibana/kibana.yml /etc/kibana/

a2enmod rewrite

service td-agent restart
service elasticsearch restart
service kibana restart
service apache2 restart

## system upgrade
apt upgrade -y

cd /vagrant/app 

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"

chmod +x ./composer.phar
rm -fr /vagrant/app/vendor
sudo -u vagrant -H ./composer.phar install
rm -f ./composer.phar
