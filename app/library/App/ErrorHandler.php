<?php

namespace App;

use Psr\Http\Message\ResponseInterface as Res;
use Psr\Http\Message\ServerRequestInterface as Req;
use Slim\Container;

class ErrorHandler
{
    /**
     * @var \Slim\Container
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function __invoke(Req $req, Res $res, \Throwable $error)
    {
        /** @var \Monolog\Logger */
        $logger = $this->container->logger;

        $code = $this->codeValidation($error->getCode()) ? $error->getCode() : 500;

        $message = $error->getMessage();

        // traceをextraに注入
        $logger->pushProcessor(function($record) use ($error) {
            $record['extra']['trace'] = $error->getTraceAsString();
            $record['extra']['class'] = get_class($error);
            return $record;
        });

        $logger->error($message, ['request' => $_REQUEST]);

        return $res->withStatus($code)
            ->write($message);
    }

    protected function codeValidation(int $code)
    {
        return $code >= 100 && $code <= 599;
    }
}
