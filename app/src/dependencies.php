<?php
// DIC configuration

use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\FluentdFormatter;
use Monolog\Handler\SocketHandler;
use Monolog\Handler\ChromePHPHandler;
use Monolog\Handler\FirePHPHandler;

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Logger($settings['name']);
    $logger->pushProcessor(new UidProcessor());

    // log to file
    $handler = new StreamHandler($settings['path'], $settings['level']);
    $logger->pushHandler($handler);

    $fluentHandler = new SocketHandler('unix:///var/run/td-agent/td-agent.sock');
    $fluentHandler->setFormatter(new FluentdFormatter);
    $logger->pushHandler($fluentHandler);

    // 開発環境用
    $displaySetting = $c->settings['displayErrorDetails'];
    if ($displaySetting) {
        $chromeHandler = new ChromePHPHandler(Logger::DEBUG);
        $firefoxHandler = new FirePHPHandler(Logger::DEBUG);
        $logger->pushHandler($chromeHandler)
            ->pushHandler($firefoxHandler);
    }

    return $logger;
};

// error handlers
$container['errorHandler'] = function($c) {
    return new \App\ErrorHandler($c);
};

$container['phpErrorHandler'] = function($c) {
    return new \App\ErrorHandler($c);
};
